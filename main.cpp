/**
 * SVM Train With OpenCV images
 *
 * Richie Steigerwald
 *
 * Copyright 2013 Richie Steigerwald <richie.steigerwald@gmail.com>
 * This work is free. You can redistribute it and/or modify it under the
 * terms of the Do What The Fuck You Want To Public License, Version 2,
 * as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.
 */

#include <iostream>

#include <opencv2/core/core.hpp> // Mat
#include <opencv2/highgui/highgui.hpp> // imread
#include <opencv2/ml/ml.hpp> // imread


// don't use cv namespace
// cv and std namespaces overlap (string, find, etc.)
using namespace std;

// print usage information
void usage(const string &program);

// reorder the training data
void sort_training_data(cv::Mat &data, cv::Mat &classes);

// create folds for the training data
void fold_training_data(const cv::Mat &data, const cv::Mat &classes, int num_folds,
                        vector<cv::Mat> &folds, vector<cv::Mat> &labels);

// normalize the fields of the training data
cv::Mat normalize_training_data(const cv::Mat &data);

/**
 * Finds optimal parameters for a support vector machine
 */
int main(int argc, const char *argv[]) {
   if (argc != 2) { usage(argv[0]); return 0; }

   int num_folds = 8;

   std::cout << "loading samples" << std::endl;

   cv::Mat samples = cv::imread(argv[1], CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_GRAYSCALE);

   std::cout << "samples loaded, normalizing data" << std::endl;

   cv::Mat normal_samples = normalize_training_data(samples.colRange(0, samples.cols - 1)); 

   std::cout << "data normalized, folding data" << std::endl;

   vector<cv::Mat> folds, labels;

   fold_training_data(normal_samples, samples.col(samples.cols - 1), num_folds,
         folds, labels);

   std::cout << "data folded, building trees" << std::endl;

   for (int i = 0; i < num_folds; i++) {
      vector<cv::Mat> train_data_folds, train_label_folds;

      for (int j = 0; j < num_folds; j++) {
         if (j != i) {
            train_data_folds.push_back(folds[j]);
            train_label_folds.push_back(labels[j]);
         } 
      } 

      cv::Mat train_data, train_labels;
      cv::vconcat(train_data_folds, train_data);
      cv::vconcat(train_label_folds, train_labels);

      std::cout << "building svm " << i << std::endl;

      cv::SVM rtrees;
      rtrees.train(train_data, train_labels);
      std::cout << "svm built, testing" << std::endl;

      cv::Mat test_data = folds[i];
      cv::Mat test_labels = labels[i];
      int correct = 0;
      for (int j = 0; j < test_labels.rows; j++) {
         float class_num = rtrees.predict(test_data.row(j));
         if (abs(class_num - test_labels.at<float>(j,0)) < 0.1) {
            correct++;
         }
      }

      std::cout << ((float)correct/(float)test_labels.rows) << std::endl;
   }
}

// Display usage information
void usage(const string &program) {
   cout << "Usage: " << program << " row_data.png" << std::endl;
}

/**
 * interleave_training_data
 * takes training data and spreads it out evenly throughout the set
 */
void fold_training_data(const cv::Mat &data, const cv::Mat &classes, int num_folds, 
                        vector<cv::Mat> &folds, vector<cv::Mat> &labels) {
   assert(data.rows == classes.rows);

   map<int, vector<int> > class_rows;

   // Set up the fold containers
   for (int i = 0; i < num_folds; i++) {
      int height = (classes.rows / num_folds) + (i < classes.rows % num_folds);
      folds.push_back(cv::Mat(height, data.cols, CV_32F));
      labels.push_back(cv::Mat(height, classes.cols, CV_32F));
   }

   // Create a list for each of the rows that correspond to each class
   for (int i = 0; i < data.rows - 1; i++) {
      int image_class = classes.at<float>(i, 0);
      class_rows[image_class].push_back(i);
   }

   // Interleave the rows across the different folds
   map<int, vector<int> >::iterator class_row;
   vector<cv::Mat>::iterator label_fold = labels.begin(), data_fold = folds.begin();
   int row = 0;
   for (class_row = class_rows.begin(); class_row != class_rows.end(); class_row++) {
      vector<int> &row_indices = class_row->second;
      assert(row_indices.size() > num_folds);

      // Distribute data from this class across folds
      for (int i = 0; i < row_indices.size(); i++) {
         if (label_fold == labels.end()) {
            label_fold = labels.begin();
            data_fold = folds.begin();
            row++;
         }
         // Copy data to fold
         classes.row(row_indices[i]).copyTo(label_fold->row(row));
         data.row(row_indices[i]).copyTo(data_fold->row(row));

         label_fold++;
         data_fold++;
      } 
   }
}

/**
 * sort_training_data
 * sorts the data in order of class label so folds can be generated more easily and evenly
 */
void sort_training_data(cv::Mat &data, cv::Mat &classes) {
   assert(data.rows == classes.rows);

   cv::Mat data_swap(1, data.cols, data.type());
   cv::Mat class_swap(1, classes.cols, classes.type());

   // Bubble Sort
   for (int i = 0; i < data.rows - 1; i++) {
      for (int j = i + 1; j < data.rows; j++) {
         if ( classes.at<float>(i, 0) > classes.at<float>(j, 0) ) {
            data.row(i).copyTo(data_swap);
            classes.row(i).copyTo(class_swap);

            data.row(j).copyTo(data.row(i));
            classes.row(j).copyTo(classes.row(i));

            data_swap.copyTo(data.row(j)); 
            class_swap.copyTo(classes.row(j)); 
         }   
      }   
   }
}

cv::Mat normalize_training_data(const cv::Mat &data) {
   cv::Mat normal_data(data.size(), CV_32F);

   for (int i = 0; i < data.cols; i++) {
      cv::Scalar mean;
      cv::Scalar stddev;
      cv::meanStdDev(data.col(i), mean, stddev);
      data.col(i).convertTo(normal_data.col(i), CV_32F, 1/stddev[0], -mean[0]);
   }

   return normal_data;
}
